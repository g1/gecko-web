async function getIdentityIndexJS(address) {
    return await api.query.identity.identityIndexOf(address)
}

async function getIdentityDataJS(idtyIndex) {
    return await api.query.identity.identities(idtyIndex)
}

async function getIdentityDataFromAddressJS(address) {
    const idtyIndex = await getIdentityIndexJS(address);
    return await api.query.identity.identities(idtyIndex)
}

async function getCurrentUdIndexJS() {
    return await api.query.universalDividend.currentUdIndex()
}

async function getPastReevalsJS() {
    const result = (await api.query.universalDividend.pastReevals()).toString()
    const obj = JSON.parse(result)
    return obj
}

async function connectNodeJS(endpoint) {
    // const { pair, json } = keyring.addUri('smart joy blossom stomach champion fun diary relief gossip hospital logic bike', 'myStr0ngP@ssworD', { name: 'mnemonic acc' });
    console.log("Connection to endpoint " + endpoint);
    const connected = await settings.connect([endpoint]);
    return connected
}

async function getBalanceJS(address) {
    const balanceR = await api.query.system.account(address);
    return balanceR['data'];
}

async function getIdentitiesNumberJS() {
    return (await api.query.identity.counterForIdentities()).toString();
}

async function getCertsJS(address) {
    const idtyIndex = await api.query.identity.identityIndexOf(address)
    const _certsReceiver = (await api.query.cert.storageIdtyCertMeta(idtyIndex.toString())).toString()
    const obj = JSON.parse(_certsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

async function subscribeNewBlocsJS(callback) {
    await api.rpc.chain.subscribeNewHeads((header) => callback(header))
}

// Get wallets presents in polkadot.js extension app
async function getInjectedAccountsJS(callback) {
    // Way 1
    if (!walletExtension.isWeb3Injected) {
        console.log("isWeb3Injected is false ! : " + walletExtension.isWeb3Injected);
        return ["no_extension"];
    }
    const allInjected = await walletExtension.web3Enable('Ğecko web app');
    if (allInjected.length === 0) {
        // no extension installed, or the user did not accept the authorization
        // in this case we should inform the use and give a link to the extension
        return ["no_extension"];
    }

    //// Way 2
    // const sleep = time => new Promise(res => setTimeout(res, time, "done sleeping"));
    // walletExtension.web3Enable('Ğecko web app');
    // await sleep(200);
    // if (!walletExtension.isWeb3Injected) {
    //     console.log("isWeb3Injected is false ! : " + walletExtension.isWeb3Injected);
    //     return ["no_extension"];
    // }

    const allAccounts = await walletExtension.web3Accounts();

    // walletExtension.web3AccountsSubscribe((injectedAccounts) => {
    //     injectedAccounts.map((account) => {
    //         callback(account);
    //     })
    // });

    return allAccounts;
}

// Pay with a wallet in polkadot.js extension app
async function payExtensionJS(callback, from, to, amount) {
    // finds an injector for an address
    const injector = await walletExtension.web3FromAddress(from);

    api.tx.balances
        .transfer(to, amount)
        .signAndSend(from, { signer: injector.signer }, (status) => {
            // console.log(status.status.toString());
            callback(status.toHuman(), from, to, amount);
        }).catch((error) => {
            console.log(':( transaction failed: ' + error);
            callback({ "error": error.toString() }, from, to, amount)
        });
}

// Check if the given address is valid
function isValidAddressJS(address) {
    const isValidAddressPolkadotAddress = () => {
        try {
            encodeAddress(
                isHex(address)
                    ? hexToU8a(address)
                    : decodeAddress(address)
            );

            return true;
        } catch (error) {
            return false;
        }
    };
    return isValidAddressPolkadotAddress();
}

////////////////////////
//////// Account ///////
////////////////////////

// import account (this is not used, prefere use polkadot.js web extension instead)
async function importAccountJS() {
    const { pair, json } = keyring.addUri(mnemonic, 'myStr0ngP@ssworD', { name: 'mnemonic acc' });
}

////////////////////////
//////// Smiths ////////
////////////////////////

//counter of membership
async function getMembershipCounterJS() {
    return (await api.query.smithsMembership.counterForMembership()).toString();
}

//counter of authorities
async function getAuthoritiesCounterJS() {
    return (await api.query.authorityMembers.authoritiesCounter()).toString();
}

//indexes of online Authorities (will be parsed List int)
async function getOnlineSmithsJS() {
    return await api.query.authorityMembers.onlineAuthorities();
}

//indexes of incoming Authorities (will be parsed List int)
async function getIncomingAuthoritiesJS() {
    return await api.query.authorityMembers.incomingAuthorities();
}

//indexes of outgoing Authorities (will be parsed List int)
async function getOutgoingAuthoritiesJS() {
    return await api.query.authorityMembers.outgoingAuthorities();
}

//Membership Expiration (will be parsed int)
async function getMembershipExpirationJS(idtyIdenx) {
    const membershipExpiration = (await api.query.membership.membership(idtyIdenx.toString())).toString();
    obj = JSON.parse(membershipExpiration);
    return obj['expireOn']
}

async function getSessionExpirationJS(idtyIdenx) {
    const sessionExpiration = (await api.query.authorityMembers.members(idtyIdenx.toString())).toString();
    
    try {
        obj = JSON.parse(sessionExpiration);
    } catch (e) {
        console.log("error with smith expir " + idtyIdenx);
        return [0,0];
    }
    return [+obj['expireOnSession'], +obj['mustRotateKeysBefore']];
}

//counter of smith certs received and issued (will be parsed List int)
async function getSmithCertsJS(idtyIndex) {
    const _smithCertsReceiver = (await api.query.smithsCert.storageIdtyCertMeta(idtyIndex.toString())).toString();
    const obj = JSON.parse(_smithCertsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

//Smith Membership Expiration (will be parsed int)
async function getSmithMembershipExpirationJS(idtyIdenx) {
    const smithMembershipExpiration = (await api.query.smithsMembership.membership(idtyIdenx.toString())).toString();

    try {
        obj = JSON.parse(smithMembershipExpiration);
    } catch (e) {
        console.log("error with smith " + idtyIdenx);
        return 0;
    }
    //if(obj.hasOwnProperty('expireOn'))
    return obj['expireOn']
}

// We use app JS object directly to test this way
async function getCurrentSessionJS() {
    return await api.query.session.currentIndex()
}

///////////////////////////////
//////// SUBSCRIPTIONS ////////
///////////////////////////////

async function subscribeNewSessionsJS(callback) {
    return await api.query.session.currentIndex((sessionNum) => callback(sessionNum))
}
