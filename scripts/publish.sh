#!/bin/bash

set -e

flutter clean && flutter pub get

if [[ $1 == "html" ]]; then
	rm -rf /home/poka/dev/flutter/gecko-web/build/web && flutter build web --web-renderer html
	ssh -p 10422 poka@pokahome.p2p.legal 'rm -rf /home/poka/gecko-html && mkdir /home/poka/gecko-html'
	rs /home/poka/dev/flutter/gecko-web/build/web poka@pokahome.p2p.legal:/home/poka/gecko-html/ 10422
else
        rm -rf /home/poka/dev/flutter/gecko-web/build/web && flutter build web --web-renderer canvaskit #auto
        ssh -p 10422 poka@pokahome.p2p.legal 'rm -rf /home/poka/gecko-web && mkdir -p /home/poka/gecko-web'
        rs /home/poka/dev/flutter/gecko-web/build/web poka@pokahome.p2p.legal:/home/poka/gecko-web/ 10422
fi
