import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';

class MyTheme with ChangeNotifier {
  bool isDark = true;

  final darkTheme = ThemeData(
    primaryColor: Colors.grey[300],
    primaryColorDark: const Color(0xFF212121),
    brightness: Brightness.dark,
    textTheme: TextTheme(
      bodyLarge: TextStyle(color: Colors.grey[300]),
      bodyMedium: TextStyle(color: Colors.grey[300]),
      titleLarge: TextStyle(color: Colors.grey[400]),
    ),
    dividerColor: Colors.black12,
    appBarTheme: const AppBarTheme(
      iconTheme: IconThemeData(color: Colors.white),
      foregroundColor: Colors.white,
      color: Color(0xFF212121),
    ),
    filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          backgroundColor: Colors.grey[800], fixedSize: const Size(250, 30)),
    ),
  );

  final lightTheme = ThemeData(
    primaryColor: Colors.white,
    primaryColorDark: backgroundColor,
    brightness: Brightness.light,
    dividerColor: Colors.white54,
    textTheme: TextTheme(
      bodyLarge: TextStyle(color: Colors.grey[900]),
      bodyMedium: TextStyle(color: Colors.grey[800]),
      titleLarge: TextStyle(color: Colors.grey[600]),
    ),
    colorScheme: ColorScheme.fromSwatch(backgroundColor: backgroundColor),
    appBarTheme: const AppBarTheme(
      iconTheme: IconThemeData(color: Colors.black),
      foregroundColor: Colors.black,
      color: Colors.white,
    ),
    filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          backgroundColor: Colors.grey[400], fixedSize: const Size(250, 30)),
    ),
  );

  ThemeData? _themeData;
  ThemeData getTheme() => _themeData!;

  MyTheme() {
    if (isDark) {
      _themeData = darkTheme;
    } else {
      _themeData = lightTheme;
    }
  }

  void switchTheme() {
    isDark = !isDark;
    _themeData = isDark ? darkTheme : lightTheme;

    notifyListeners();
  }
}
