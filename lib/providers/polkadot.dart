// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/wallet_data.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/polkadot_lib.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:provider/provider.dart';
import 'dart:js' as js;

class PolkadotProvider with ChangeNotifier {
  bool nodeConnected = false;
  int nbrIdentity = 0;
  Map<String, List<int>> certsCounterCache = {};
  final indexer = Indexer();
  Map<String, WalletData> listMyWallets = {};
  late WalletData selectedWallet;
  bool? haveExtension;
  Map<String, double> balanceCache = {};
  Map transactionStatus = {};

  Future<Map<String, double>> getBalance(String address) async {
    if (!nodeConnected) {
      return {
        'transferableBalance': 0,
        'free': 0,
        'unclaimedUds': 0,
        'reserved': 0,
      };
    }

    // Get onchain storage values
    // In Gecko web, this is a bit tricky cause promiseToFuture convert, and dynamic JS type, making null values difficult to catch
    final Map balanceGlobal =
        json.decode((await promiseToFuture(getBalanceJS(address))).toString());
    final int idtyIndex = await getIdentityIndex(address);
    final Map? idtyData = idtyIndex == -1
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());
    final int currentUdIndex =
        int.parse((await promiseToFuture(getCurrentUdIndexJS())).toString());
    final List pastReevals = (await promiseToFuture(getPastReevalsJS()));

    // Compute amount of claimable UDs
    final int unclaimedUds = _computeUnclaimUds(currentUdIndex,
        idtyData?['data']?['firstEligibleUd'] ?? 0, pastReevals);

    // Calculate transferable and potential balance
    final int transferableBalance = (balanceGlobal['free'] + unclaimedUds);

    Map<String, double> finalBalances = {
      'transferableBalance': transferableBalance / 100,
      'free': balanceGlobal['free'] / 100,
      'unclaimedUds': unclaimedUds / 100,
      'reserved': balanceGlobal['reserved'] / 100,
    };

    return finalBalances;
  }

  int _computeUnclaimUds(
      int currentUdIndex, int firstEligibleUd, List pastReevals) {
    int totalAmount = 0;

    if (firstEligibleUd == 0) return 0;

    for (final List reval in pastReevals.reversed) {
      final int revalNbr = reval[0];
      final int revalValue = reval[1];

      // Loop each UDs revaluations and sum unclaimed balance
      if (revalNbr <= firstEligibleUd) {
        final count = currentUdIndex - firstEligibleUd;
        totalAmount += count * revalValue;
        break;
      } else {
        final count = currentUdIndex - revalNbr;
        totalAmount += count * revalValue;
        currentUdIndex = revalNbr;
      }
    }

    return totalAmount;
  }

  Future<int> getIdentitiesNumber() async {
    final promise = getIdentitiesNumberJS();
    nbrIdentity = int.parse(await promiseToFuture(promise));
    // log.d(nbrIdentity);
    notifyListeners();
    return nbrIdentity;
  }

  Future<List<int>> getCerts(String address) async {
    if (nodeConnected) {
      final promise = getCertsJS(address);
      final certsJS = (await promiseToFuture(promise));
      final certs = [
        int.parse(certsJS[0].toString()),
        int.parse(certsJS[1].toString())
      ];

      certsCounterCache.update(address, (value) => certs,
          ifAbsent: () => certs);
      return certs;
    } else {
      return [];
    }
  }

  Future<String> connectNode(BuildContext context) async {
    final polkaSubBlock =
        Provider.of<PolkadotSubscribBlocksProvider>(context, listen: false);
    final polkaSubSession =
        Provider.of<PolkadotSubscribSessionsProvider>(context, listen: false);
    final promise = connectNodeJS(duniterEndpoint);
    final res = (await promiseToFuture(promise)).toString();
    if (res != 'null') {
      polkaSubBlock.subscribeNewBlocs();
      polkaSubSession.subscribeNewSessions();
      await getIdentitiesNumber();
      await getBlockStart();
      nodeConnected = true;
      notifyListeners();
    }
    return res;
  }

  Future<DateTime> getBlockStart() async {
    final result = await indexer.execQuery(getBlockchainStartQ, {});
    if (!result.hasException) {
      startBlockchainTime =
          DateTime.parse(result.data!['block'][0]['created_at']);
      return startBlockchainTime;
    }
    return DateTime(0, 0, 0, 0, 0);
  }

  Future<int> getIdentityIndex(String address) async {
    final idtyIndexJS = await promiseToFuture(getIdentityIndexJS(address));
    return int.parse(
        idtyIndexJS.toString() == '' ? '-1' : idtyIndexJS.toString());
  }

  Future<Map<String, WalletData>> getInjectedAccounts() async {
    final callbackJS = js.allowInterop(_getInjectedAccountsCallBack);
    final accountsJS =
        dartify(await promiseToFuture(getInjectedAccountsJS(callbackJS)))
            as List;
    if (accountsJS.isEmpty) {
      listMyWallets = {};
      haveExtension = true;
      notifyListeners();
      return {};
    }

    if (accountsJS[0] == 'no_extension') {
      log.i('No extension installed');
      haveExtension = false;
      notifyListeners();
      return {};
    }
    haveExtension = true;
    for (int i = 0; i < accountsJS.length; i++) {
      if (accountsJS[i]['meta']?['source'] == "polkadot-js") {
        await updateListMyWallets(accountsJS[i]);
      }
    }
    // Pour tester:
    updateListMyWallets({
      'address': '5CQ8T4qpbYJq7uVsxGPQ5q2df7x3Wa4aRY6HUWMBYjfLZhnn',
      'name': 'Poka'
    });

    selectedWallet = listMyWallets.values.first;

    //log.d(listMyWallets);
    notifyListeners();
    return listMyWallets;
  }

  void _getInjectedAccountsCallBack(var resultJS) {
    final accountsJS = dartify(resultJS) as Map;

    // log.d(accountsJS);
    if (accountsJS['meta']?['source'] == "polkadot-js") {
      updateListMyWallets(accountsJS);
      selectedWallet = listMyWallets.values.first;

      notifyListeners();
      // log.d(listMyWallets.values);
    }
  }

  Future updateListMyWallets(Map account) async {
    // log.d('tataaii: ' + account['address']);
    final int idtyIndex = await getIdentityIndex(account['address']);
    final Map? idtyData = idtyIndex == -1
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());

    late IdentityStatus idtyStatus;

    if (idtyData == null) {
      idtyStatus = IdentityStatus.none;
    } else if (idtyData['status'] == 'Created') {
      idtyStatus = IdentityStatus.created;
    } else if (idtyData['status'] == 'Confirmed') {
      idtyStatus = IdentityStatus.confirmed;
    } else if (idtyData['status'] == 'Validated') {
      idtyStatus = IdentityStatus.validated;
    }

    late final newAccountData = WalletData(
        address: account['address'],
        name: account['meta']?['name'] ?? 'unnamed',
        status: idtyStatus);
    listMyWallets.update(account['address'], (_) => newAccountData,
        ifAbsent: () => newAccountData);
  }

  Future payExtension(String from, String to, double amount) async {
    final amountUnit = int.parse((amount * 100).toStringAsFixed(0));
    final callbackJS = js.allowInterop(_payExtensionCallback);
    try {
      js.context
          .callMethod('payExtensionJS', [callbackJS, from, to, amountUnit]);
    } catch (e) {
      log.e(e);
    }
  }

  void _payExtensionCallback(var resultJS, var from, var to, var amount) {
    final homeProvider = Provider.of<HomeProvider>(homeContext, listen: false);
    final Map result = _toDartSimpleObject(resultJS);
    if (result.containsKey('error')) {
      log.d(result['error']);
    } else {
      // log.d(result);
      homeProvider.payAmount.text = '';
      late String status;
      if (result['status'] == null) {
        status = 'none';
      } else if (result['status'] is String) {
        status = result['status'];
      } else if (result['status'] is Map) {
        status = result['status'].keys.first;
      } else {
        status = 'unknown';
      }

      Map formatStatus(String direction) => {
            'from': from,
            'to': to,
            'amount': amount,
            'direction': direction,
            'status': status,
            'error': result['dispatchError']
          };

      transactionStatus.update(from, (value) => formatStatus('sender'),
          ifAbsent: () => formatStatus('sender'));

      transactionStatus.update(to, (value) => formatStatus('receiver'),
          ifAbsent: () => formatStatus('receiver'));
      // log.d(transactionStatus[from]);

      notifyListeners();
    }
  }

  void reload() {
    notifyListeners();
  }
}

_toDartSimpleObject(thing) {
  if (thing is js.JsArray) {
    List res = [];
    js.JsArray a = thing;
    for (var otherthing in a) {
      res.add(_toDartSimpleObject(otherthing));
    }
    return res;
  } else if (thing is js.JsObject) {
    Map res = {};
    js.JsObject o = thing;
    js.JsArray<dynamic> k = js.context['Object'].callMethod('keys', [o]);
    for (var k in k) {
      res[k] = _toDartSimpleObject(o[k]);
    }
    return res;
  } else {
    return thing;
  }
}
