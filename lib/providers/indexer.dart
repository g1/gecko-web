import 'package:gecko_web/global.dart';
import 'package:gecko_web/queries.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Indexer {
  final GraphQLClient client;
  Indexer._(this.client);

  factory Indexer() {
    final httpLink = HttpLink('$indexerEndpoint/v1/graphql');

    final client = GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    );
    return Indexer._(client);
  }

  factory Indexer.relay() {
    final httpLink = HttpLink('$indexerEndpoint/v1beta1/relay');

    final client = GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    );
    return Indexer._(client);
  }

  Future<String> nameByAddress(String address) async {
    String username = "";
    //TODO: ca beugue ici: expecting a value for non-nullable variable: "name"
    final result = await execQuery(getNameByAddressQ, {'name': address});
    if (result.hasException) {
      log.d(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      final Map<String, dynamic>? data = result.data?['account_by_pk'];

      if (data != null && data.containsKey('identity')) {
        final Map<String, dynamic> identity = data['identity'];
        username = identity['name'];
      }
    }
    return username;
  }

  Future<Map<String, String>> namesByAddress(List<String> addresses) async {
    Map<String, String> mapNames = {};

    final result =
        await execQuery(getListNameByAddressQ, {'listPubKey': addresses});

    if (result.hasException) {
      log.d(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      final List res = result.data?['account'] ?? [];
      for (int i = 0; i < res.length; i++) {
        mapNames.putIfAbsent(
            res[i]['pubkey']!, () => res[i]['identity']['name']!);
      }
    }

    return mapNames;
  }

  Future<QueryResult> execQuery(String query,
      [Map<String, dynamic> variables = const {}]) async {
    final QueryOptions options =
        QueryOptions(document: gql(query), variables: variables);

    return await client.query(options);
  }

  Future debugGql() async {
    final result = await execQuery(
        latestTransactionsPagedQ, {'cursor': null, 'number': 250});

    log.d(result.data!['transaction_connection']['edges'].length);
  }
}
