// ignore_for_file:  depend_on_referenced_packages

@JS()
library polkadot_lib;

import 'package:js/js.dart';

@JS()
external final dynamic api;
external connectNodeJS(String endpoint);
external Map getBalanceJS(String address);
external List<int> getCertsJS(String address);
external int getIdentityIndexJS(String address);
external Map getIdentityDataJS(int idtyIndex);
external Map getIdentityDataFromAddressJS(String address);
external int getCurrentUdIndexJS();
external List<List> getPastReevalsJS();
external int getIdentitiesNumberJS();
external getInjectedAccountsJS(var callback);
external bool isValidAddressJS(String address);

// Smiths
external int getMembershipCounterJS();
external int getAuthoritiesCounterJS();
external List<int> getOnlineSmithsJS();
external List<int> getIncomingAuthoritiesJS();
external List<int> getOutgoingAuthoritiesJS();
external int getMembershipExpirationJS(int idtyIndex);
external List<int> getSessionExpirationJS(int idtyIndex);
external List<int> getSmithCertsJS(int idtyIndex);
external int getSmithMembershipExpirationJS(int idtyIndex);
external int getCurrentSessionJS();
