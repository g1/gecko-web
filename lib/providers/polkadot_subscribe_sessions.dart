// ignore_for_file: avoid_web_libraries_in_flutter

import 'package:flutter/material.dart';
import 'dart:js' as js;

class PolkadotSubscribSessionsProvider with ChangeNotifier {
  int sessionNumber = 0;

  Future subscribeNewSessions() async {
    var callbackJS = js.allowInterop(newSessionsCallback);
    js.context.callMethod('subscribeNewSessionsJS', [callbackJS]);
  }

  void newSessionsCallback(js.JsObject ev) {
    sessionNumber = int.parse(ev.toString());
    notifyListeners();
  }
}
