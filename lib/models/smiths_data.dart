import 'package:flutter/material.dart';

enum SmithStatus {
  online,
  offline,
  incoming,
  outgoing,
  certified,
  requested,
}

class SmithData {
  String address;
  int idtyIndex;
  String? username;
  SmithStatus? status;
  List<int>? memberCerts;
  List<int>? smithCerts;
  Color? tileColor;
  int? memberCertifBlockExpir;
  int? smithCertifBlockExpir;
  int? sessionKeyExpir;
  int? mustRotateKeys;

  SmithData({
    required this.address,
    required this.idtyIndex,
    this.username,
    this.status,
    this.memberCerts,
    this.smithCerts,
    this.tileColor,
    this.memberCertifBlockExpir,
    this.smithCertifBlockExpir,
    this.sessionKeyExpir,
    this.mustRotateKeys,
  });
}
