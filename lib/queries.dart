const String latestTransactionsPagedQ = r'''
query ($cursor: String, $number: Int!)  {
  transaction_connection(order_by: {created_at: desc}, first: $number, after: $cursor) {
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      node {
        issuer_pubkey
        receiver_pubkey
        created_at
        issuer {
          identity {
            name
          }
        }
        receiver {
          identity {
            name
          }
        }
        amount
      }
    }
  }
}
''';

const String latestIdentityPagedQ = r'''
query ($cursor: String, $number: Int!)  {
  identity_connection(where: {name: {_is_null: false}}, order_by: {confirmed_at: desc}, first: $number, after: $cursor) {
    edges {
      node {
        pubkey
        name
        confirmed_at
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
''';

const String getNameByAddressQ = r'''
query ($name: String!) {
  account_by_pk(pubkey: $name) {
    identity {
      name
    }
    pubkey
  }
}
''';

const String searchAddressByNameQ = r'''
query ($name: String!) {
  search_identity(args: {name: $name}) {
    pubkey
    name
  }
}
''';

const String getHistoryByAddressQ = r'''
query ($address: String!, $number: Int!, $cursor: String) {
  transaction_connection(where: 
  {_or: [
    {issuer_pubkey: {_eq: $address}}, 
    {receiver_pubkey: {_eq: $address}}
  ]}, 
  order_by: {created_at: desc},
  first: $number,
  after: $cursor) {
    edges {
      node {
        amount
        created_at
        issuer_pubkey
        receiver_pubkey
        issuer {
          identity {
            name
          }
        }
        receiver {
          identity {
            name
          }
        }
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
''';

const String getListNameByAddressQ = r'''
query ($listPubKey: [String!]) {
    account(where: {pubkey: {_in: 
        $listPubKey }} )
    {
      pubkey
      identity{
        name
      }
    } 
}
''';

const String getBlockchainStartQ = r'''
query {
  block(limit: 1) {
    created_at
    number
  }
}
''';

const String getCertsReceived = r'''
query ($address: String!) {
  certification(where: {receiver: {pubkey: {_eq: $address}}}) {
    issuer {
      pubkey
      name
    }
    created_at
  }
}
''';

const String getCertsSent = r'''
query ($address: String!) {
  certification(where: {issuer: {pubkey: {_eq: $address}}}) {
    receiver {
      pubkey
      name
    }
    created_at
  }
}
''';
