import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:truncate/truncate.dart';

int minutesToBlocks(int minutes) => (minutes * 60 ~/ 6);
int hoursToBlocks(int hours) => (minutesToBlocks(hours) * 60);
int daysToBlocks(int days) => (hoursToBlocks(days) * 24);
int monthsToBlocks(int months) => (daysToBlocks(months) * 30);
int weekToBlocks(int week) => (daysToBlocks(week) * 7);

int hoursToSession(int hours) => (hours);
int daysToSessions(int days) => (days * 24);
int monthsToSessions(int months) => (daysToSessions(months) * 30);
int weekToSessions(int week) => (daysToSessions(week) * 7);

String getShortPubkey(String pubkey) {
  String pubkeyShort = truncate(pubkey, 7,
          omission: String.fromCharCode(0x2026),
          position: TruncatePosition.end) +
      truncate(pubkey, 6, omission: "", position: TruncatePosition.start);
  return pubkeyShort;
}

snackCopyKey(BuildContext context, String address) {
  Clipboard.setData(ClipboardData(text: address));

  final snackBar = SnackBar(
      padding: const EdgeInsets.all(20),
      content: Text("thisAddressHasBeenCopiedToClipboard".tr(),
          style: const TextStyle(fontSize: 16)),
      duration: const Duration(seconds: 2));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

DateTime blocNumberToDate(int blocNumber) {
  return startBlockchainTime.add(Duration(seconds: blocNumber * 6));
}

DateTime sessionNumberToDate(int sessionNumber) {
  return startBlockchainTime.add(Duration(hours: sessionNumber));
}

final Map<int, String> monthsInYear = {
  1: "month1".tr(),
  2: "month2".tr(),
  3: "month3".tr(),
  4: "month4".tr(),
  5: "month5".tr(),
  6: "month6".tr(),
  7: "month7".tr(),
  8: "month8".tr(),
  9: "month9".tr(),
  10: "month10".tr(),
  11: "month11".tr(),
  12: "month12".tr()
};

bool isAddress(String address) {
  final RegExp regExp = RegExp(
    r'^[a-zA-Z0-9]+$',
    caseSensitive: false,
    multiLine: false,
  );

  if (regExp.hasMatch(address) == true &&
      address.length > 45 &&
      address.length < 52) {
    return true;
  } else {
    return false;
  }
}
