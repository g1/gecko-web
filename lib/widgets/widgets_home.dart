import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/widgets/list_latest_transactions.dart';
import 'package:gecko_web/widgets/list_latest_identities.dart';
import 'package:provider/provider.dart';

class InfosView extends StatelessWidget {
  const InfosView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: screenHeight,
      child: Column(
        children: [
          const SizedBox(height: 30),
          const Text(
            'Duniter: $duniterEndpoint',
            style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12),
          ),
          const SizedBox(height: 10),
          Text(
            'Indexer: $indexerEndpoint',
            style: const TextStyle(fontWeight: FontWeight.w100, fontSize: 12),
          ),
          const SizedBox(height: 10),
          Consumer<PolkadotSubscribBlocksProvider>(
              builder: (context, polkaBlock, _) {
            return Text(
              polkaBlock.blockNumber == 0
                  ? ''
                  : 'Block: ${polkaBlock.blockNumber}',
              style: TextStyle(
                  fontSize: 14,
                  color: Theme.of(context).textTheme.bodyLarge!.color),
            );
          }),
          const Spacer(),
          const LatestIdentityWidget(),
          const SizedBox(height: 20),
          const LatestTransactionsWidget(),
        ],
      ),
    );
  }
}
