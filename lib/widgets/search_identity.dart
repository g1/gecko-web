import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/polkadot_lib.dart';
import 'package:gecko_web/providers/search.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:gecko_web/widgets/cert_tile.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class SearchIdentity extends StatelessWidget {
  const SearchIdentity({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final searchProvider = Provider.of<SearchProvider>(context, listen: false);

    final httpLink = HttpLink(
      '$indexerEndpoint/v1/graphql',
    );

    final client = ValueNotifier(
      GraphQLClient(
        cache: GraphQLCache(),
        link: httpLink,
      ),
    );

    return Consumer<SearchProvider>(builder: (context, search, _) {
      // bool isAddressBool = isAddress(searchProvider.searchController.text);
      bool isAddressBool =
          isValidAddressJS(searchProvider.searchController.text);

      return GraphQLProvider(
        client: client,
        child: Query(
            options: QueryOptions(
              document:
                  gql(isAddressBool ? getNameByAddressQ : searchAddressByNameQ),
              variables: {
                'name': searchProvider.searchController.text,
              },
              // pollInterval: const Duration(seconds: 10),
            ),
            builder: (QueryResult result,
                {VoidCallback? refetch, FetchMore? fetchMore}) {
              // if (result.data == null) {
              //   return const SizedBox();
              // }
              if (result.hasException) {
                return Text(result.exception.toString());
              }

              // if (result.isLoading) {
              //   isLoading = true;
              //   notifyListeners();
              // } else {
              //   isLoading = false;
              //   notifyListeners();
              // }

              if (isAddressBool) {
                log.d(searchProvider.searchController.text);
                // log.d(result.data?['account_by_pk']);

                final res = result.data?['account_by_pk'];
                res == null
                    ? searchProvider.searchResult = [
                        {"pubkey": searchProvider.searchController.text}
                      ]
                    : searchProvider.searchResult = [res];
              } else {
                searchProvider.searchResult =
                    result.data?['search_identity'] ?? [];
              }

              if (searchProvider.searchResult.isEmpty) {
                return const Text('');
              }

              int keyID = 0;
              return SizedBox(
                height: searchProvider.searchResult.length * 75,
                width: 610,
                // color: Theme.of(context).primaryColorDark.withOpacity(0.7),
                child: ListView(children: <Widget>[
                  for (Map profile in searchProvider.searchResult)
                    Material(
                      color:
                          Theme.of(context).primaryColorDark.withOpacity(0.8),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: ListTile(
                            key: Key('searchResult${keyID++}'),
                            hoverColor: Colors.black.withOpacity(1),
                            horizontalTitleGap: 40,
                            contentPadding: const EdgeInsets.all(5),
                            leading: defaultAvatar(40),
                            title: Row(children: <Widget>[
                              Text(
                                  profile['name'] ??
                                      profile['identity']?['name'] ??
                                      '',
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyMedium!
                                          .color,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                  textAlign: TextAlign.center),
                            ]),
                            trailing: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Balance(
                                      address: profile['pubkey'],
                                      size: 14,
                                      color: Theme.of(context)
                                          .textTheme
                                          .titleLarge!
                                          .color!)
                                ]),
                            subtitle: Row(children: <Widget>[
                              Text(getShortPubkey(profile['pubkey'] ?? ''),
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .titleLarge!
                                          .color,
                                      fontSize: 14,
                                      fontFamily: 'Monospace',
                                      fontWeight: FontWeight.w500),
                                  textAlign: TextAlign.center),
                            ]),
                            dense: false,
                            isThreeLine: false,
                            onTap: () {
                              homeProvider.currentAddress = profile['pubkey'];
                              homeProvider.currentName = profile['name'] ?? '';
                              homeProvider.reload();
                              if (screenWidth < screenBreakpoint) {
                                Scaffold.of(context).openEndDrawer();
                              }
                            }),
                      ),
                    ),
                ]),
              );
            }),
      );
    });
  }
}
