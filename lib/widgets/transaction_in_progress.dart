import 'package:fade_and_translate/fade_and_translate.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:provider/provider.dart';

class TransactionInProgress extends StatelessWidget {
  const TransactionInProgress({super.key, required this.address});

  final String address;

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(homeContext, listen: false);
    return Consumer<PolkadotProvider>(builder: (context, polka, _) {
      final myStatus = polka.transactionStatus[address];
      if (myStatus == null) {
        return const SizedBox();
      }
      final isVisible = myStatus['status'] != 'Finalized';

      late Widget statusWidget;
      late String humanStatus;
      if (myStatus['error'] != null) {
        statusWidget = const Icon(Icons.close, color: Colors.red);
        humanStatus = ":( Une erreur s'est produite :(";
      } else if (myStatus['status'] == 'InBlock') {
        statusWidget = const Icon(Icons.done, color: Colors.green);
        humanStatus = "Transaction enregistrée";
      } else if (myStatus['status'] == 'Finalized') {
        statusWidget = const Icon(Icons.done_all, color: Colors.green);
        humanStatus = "Transaction confirmé !";
      } else {
        statusWidget = const Loading(size: 20, stroke: 3);
        humanStatus = "Transaction en cours...";
      }

      final isSender = myStatus['direction'] == 'sender';
      final finalAmount =
          (isSender ? myStatus['amount'] * -1 : myStatus['amount']) / 100;

      return FadeAndTranslate(
        visible: isVisible,
        translate: const Offset(0, -40),
        delay: const Duration(seconds: 3),
        duration: const Duration(milliseconds: 700),
        // autoStart: true,
        // autoStartDelay: Duration.zero,
        // curve: Curves.,
        onCompleted: () => homeProvider.reload(),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: orangeC,
              width: 1,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 10),
              const Text(
                'Transaction en cours',
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.w500),
              ),
              const SizedBox(height: 5),
              ListTile(
                  key: const Key('transactionInProgress'),
                  contentPadding: const EdgeInsets.only(
                      left: 20, right: 30, top: 15, bottom: 15),
                  leading: statusWidget,
                  title: Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(
                        getShortPubkey(myStatus[isSender ? 'to' : 'from']),
                        style: const TextStyle(
                            fontSize: 16, fontFamily: 'Monospace')),
                  ),
                  subtitle: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey[700],
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Maintenant',
                            style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .titleLarge!
                                    .color)),
                        TextSpan(
                          text: '  ·  ',
                          style: TextStyle(
                            fontSize: 20,
                            color:
                                Theme.of(context).textTheme.titleLarge!.color,
                          ),
                        ),
                        TextSpan(
                          text: humanStatus,
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color:
                                Theme.of(context).textTheme.titleLarge!.color,
                          ),
                        ),
                      ],
                    ),
                  ),
                  trailing: Text("$finalAmount $currencyName",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: isSender ? Colors.blue : Colors.green),
                      textAlign: TextAlign.justify),
                  dense: false,
                  isThreeLine: false),
              const SizedBox(height: 5),
            ],
          ),
        ),
      );
    });
  }
}
