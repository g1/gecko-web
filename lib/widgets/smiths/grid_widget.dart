import 'package:flutter/material.dart';
import 'package:gecko_web/widgets/smiths/smith_tile.dart';

class GridWidget extends StatelessWidget {
  const GridWidget({
    required this.nTule,
    required this.smithsTiles,
    super.key,
  });

  final int nTule;
  final List<SmithTile> smithsTiles;

  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      primary: false,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //childAspectRatio: 1,
        crossAxisCount: nTule,
        mainAxisExtent: 50,
        crossAxisSpacing: 20, // vertical spacing
        mainAxisSpacing: 20, // horizontal spacing
      ),
      children: smithsTiles,
      //     //uncomment lines below to test with many tiles
      //     List.generate(
      //   20,
      //   (index) => Container(
      //     decoration: const BoxDecoration(
      //         color: yellowC,
      //         borderRadius: BorderRadius.all(Radius.circular(20))),
      //     child: Column(
      //       mainAxisAlignment: MainAxisAlignment.center,
      //       children: [
      //         Text(
      //           '$index',
      //           textAlign: TextAlign.center,
      //         ),
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}
