import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';

class Counters extends StatelessWidget {
  const Counters({
    required this.nbrAutorities,
    required this.nbrMembers,
    required this.nbrCertified,
    required this.nbrToBeCertified,
    super.key,
  });

  final int nbrAutorities;
  final int nbrMembers;
  final int nbrCertified;
  final int nbrToBeCertified;

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Flex(
            direction: Axis.vertical,
            children: [
              Container(
                decoration: const BoxDecoration(
                    color: yellowC,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                width: screenWidth / 4,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 8.0),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        //height: 30,
                        decoration: const BoxDecoration(
                            color: orangeC,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 5.0),
                          child: Row(
                            children: [
                              Text('  ${"online".tr()}: $nbrAutorities',
                                  style: const TextStyle(fontSize: 19)),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        //height: 30,
                        decoration: const BoxDecoration(
                            color: orangeC,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 5.0),
                          child: Row(
                            children: [
                              Text(
                                  '  ${"offline".tr()}: ${nbrMembers - nbrAutorities}',
                                  style: const TextStyle(fontSize: 19)),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        //height: 30,
                        decoration: const BoxDecoration(
                            color: orangeC,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 5.0),
                          child: Row(
                            children: [
                              Text('  ${"certified".tr()}: $nbrCertified',
                                  style: const TextStyle(fontSize: 19)),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        //height: 30,
                        decoration: const BoxDecoration(
                            color: orangeC,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 5.0),
                          child: Row(
                            children: [
                              Text('  ${"requested".tr()}: $nbrToBeCertified',
                                  style: const TextStyle(fontSize: 19)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
