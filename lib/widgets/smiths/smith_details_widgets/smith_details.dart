import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/certs_widget.dart';

import 'package:gecko_web/widgets/smiths/smith_details_widgets/session_expiration.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/smith_colors.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class SmithDetails extends StatelessWidget {
  const SmithDetails({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<Smiths>(context, listen: false);
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    final smith = smithsP.currentSmithDetails;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Text(
          smith.username!,
          style: mainStyle,
        ),
        GestureDetector(
          key: const Key('copyPubkey'),
          onTap: () {
            Clipboard.setData(ClipboardData(text: smith.address));
            snackCopyKey(context, smith.address);
          },
          child: Text(
            getShortPubkey(smith.address),
            style: mainStyle,
          ),
        ),
        GestureDetector(
          key: const Key('copyPubkeyBis'),
          onTap: () {
            Clipboard.setData(ClipboardData(text: smith.address));
            snackCopyKey(context, smith.address);
          },
          child: QrImageWidget(
            data: smith.address,
            version: QrVersions.auto,
            size: 120,
          ),
        ),
        Text(
          smith.idtyIndex.toString(),
          style: mainStyle16,
        ),
        Text(
          'membercertif'.tr(),
          style: mainStyle,
        ),
        InkWell(
          onTap: () {
            smithsP.showCertsView = true;
            smithsP.reload();
          },
          child: CertsWidget(
            smith: smith,
            type: "member",
            getExpiration: smithsP.getMembershipExpiration(smith),
            getCerts: polka.getCerts(smith.address),
            icon: 'assets/medal.png',
          ),
        ),
        // TODO: Expiration certif have to be displayed in certifications.dart
        // ExpirationWidget(
        //     getExpiration: smithsP.getMembershipExpiration(smith),
        //     smith: smith),
        Text(
          'smithcertif'.tr(),
          style: mainStyle,
        ),
        CertsWidget(
          smith: smith,
          type: "smith",
          getExpiration: smithsP.getSmithMembershipExpiration(smith),
          getCerts: smithsP.getSmithCerts(smith),
          icon: 'assets/server.png',
        ),
        // TODO: Expiration certif have to be displayed in certifications.dart when compatible with smith
        // ExpirationWidget(
        //     getExpiration: smithsP.getSmithMembershipExpiration(smith),
        //     smith: smith),
        SessionExpirationWidget(smith: smith),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("${"status".tr()}: ", style: mainStyle),
            Text(
              // TODO: rendre traduisible cet affichage ?
              "${smith.status?.name}",
              style: TextStyle(
                fontSize: 20,
                decoration: TextDecoration.none,
                color: onlineColor(smith.status!),
              ),
            ),
          ],
        ),
        if (polka.listMyWallets
            .containsKey(smithsP.currentSmithDetails.address))
          Column(children: [
            if (smith.status == SmithStatus.online)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          //side: const BorderSide(color: Colors.red),
                        ),
                        backgroundColor: Colors.orange,
                        fixedSize: const Size(200, 30)),
                    onPressed: () {},
                    child: const Row(
                      children: [
                        Expanded(
                          flex: 8,
                          child: Text('setSessionKeys',
                              textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 2,
                          child: Icon(
                            Icons.refresh,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        backgroundColor: Colors.red,
                        fixedSize: const Size(200, 30)),
                    onPressed: () {},
                    child: const Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text('goOffline', textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 2,
                          child: Icon(
                            Icons.arrow_downward,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            if (smith.status == SmithStatus.offline)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        backgroundColor: Colors.green,
                        fixedSize: const Size(200, 30)),
                    onPressed: () {},
                    child: const Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text('goOnline', textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 2,
                          child: Icon(
                            Icons.arrow_upward,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        backgroundColor: Colors.red,
                        fixedSize: const Size(200, 30)),
                    onPressed: () {},
                    child: const Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text('revokeMembership',
                              textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 2,
                          child: Icon(
                            Icons.arrow_downward,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            if (smith.status == SmithStatus.certified)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        backgroundColor: Colors.green,
                        fixedSize: const Size(200, 30)),
                    onPressed: () {},
                    child: const Row(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Text('claimMembership',
                              textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 2,
                          child: Icon(
                            Icons.arrow_upward,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
          ]),
      ]),
    );
  }
}
