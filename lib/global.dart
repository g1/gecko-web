import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';

const String currencyName = 'ĞD';
const String duniterEndpoint = "wss://gdev.p2p.legal/ws";
const screenBreakpoint = 1130;
late String appVersion;

// Colors
const Color orangeC = Color(0xffd07316);
const Color yellowC = Color(0xffFFD68E);
const Color floattingYellow = Color(0xffEFEFBF);
const Color backgroundColor = Color(0xFFF5F5F5);

// Style
TextStyle mainStyle = const TextStyle(
  fontSize: 20,
  decoration: TextDecoration.none,
  color: Colors.black,
);
TextStyle mainStyle16 = const TextStyle(
  fontSize: 16,
  decoration: TextDecoration.none,
  color: Colors.black,
);

// Screen size
late double screenWidth;
late double screenHeight;

late BuildContext homeContext;

// Logger
var log = Logger();

String indexerEndpoint = '';

// Hive box
late Box configBox;

// Indexer
late DateTime startBlockchainTime;

//Hardcode members while waiting for the data from the indexer
List<int> listIndexGenesis = [
  6951,
  6317,
  //2453,
  2457,
  //7238,
  7139,
  1,
  //3594,
  3595
];
List<int> listIndexRequestedSmith = [7228, 2457, 6951, 7237, 7174];
List<String> listAddressGenesis = [
  "5CQ8T4qpbYJq7uVsxGPQ5q2df7x3Wa4aRY6HUWMBYjfLZhnn",
  "5CtmAELWQ6FGDrAfsfVbGe4CXW4YrpNRTVU27eWzSE6J8ocU",
  "5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz",
  "5D2DnScFpxoEUXDwZbJH18tRsQMygBSh1F6YCcWvTYzKY2W7",
  "5CCrBS67BrpBx3ihGHc72HZp3eHHbETxWFuNfwbbdoGSJFN8",
  "5DUjwHRqPayt3tAZk1fqEgU99xZB9jzBHKy2sMSTNcc7m9D1"
];
List<String> listAddressRequestedSmith = [
  "5GBVhdJUdsGhxozu6R8X6x2pTZvuuW46s7JSU4tiW7Zd3WmY",
  "5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz",
  "5CQ8T4qpbYJq7uVsxGPQ5q2df7x3Wa4aRY6HUWMBYjfLZhnn",
  "5E58CLNXPFpBWrygp3ry8HyS2hiuQpRuXbZFUaTq7q9gG3Vv",
  "5FH48744BHgNoLBe8syGXbTEnSpGhp8ttdAKW4MWcR7TUKai"
];

// keys
final scaffoldKey = GlobalKey<ScaffoldState>();
